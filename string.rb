#! /usr/bin/env ruby
# -*- coding: utf-8 -*-

# This is my own String-extention to get some basic color-support

class String
  def green
    "\e[38;5;10m" + self + "\033[0m"
  end

  def red
    "\e[38;5;1m" + self + "\033[0m"
  end

  def yellow
    "\e[38;5;11m" + self + "\033[0m"
  end

  def blue
    "\e[38;5;27m" + self + "\033[0m"
  end

  def pink
    "\e[38;5;199m" + self + "\033[0m"
  end

  def iso_to_utf8
    self.encode('iso-8859-1').force_encoding('utf-8')
  end

end
